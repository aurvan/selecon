**About Selecon**

Selecon (Selenium Connector) is a free IDE for Selenium scripts on Mac OSX and Windows Native Platform. With this tool you can create, edit, save and run automated test cases for web application with easy to use UI and controls. You can also open, work with and run test cases recorded and saved by Selenium IDE (Firefox Extension).


**Key Features**

* Free: Selecon is totally free. No trial version, no demo version, you will get a fully functional application.
* Regular Updates: We already have planned and started working on features for next releases. Also there will be bug fixing as reported by our QA people and app users.
* Run Test Cases On Any Browser: You can run your test cases on most of the popular browsers including Firefox, Chrome, Safari, Internet Explorer etc. Also Selecon is available on Mac OSX and Windows platform that adds to combinations.
* Run Single Test Case Or Entire Test Suite: Selecon provides a feature that allows you to run single test case independently or entire test suite, just long press on Run button on tool bar to select run type.
* Run, Pause, Stop Test Case Any Time: Selecon provides controls to pause test run in between, so you can take a look at the output of any step. You can also stop test execution if you want.
* Beautiful Looks And Carefully Designed, Clean And Neat Layout: While designing Selecon we took due care to make it easy to understand and use. We have kept it simple, neat and of course beautiful.
* Features To Make Your Task Easy: Selecon provides you various keyboard shortcuts, tooltips, input suggestions to ease your task.
	- You can open a test suite or test case with (Command O), add a test case with (Command Shift N), add a step with (Command N) etc.
	- You can drag and drop test case or step to alter its execution index.
	- You will have auto complete suggestions for test step command as you type.
	- Hover mouse pointer over overflow truncated text to see full text in tooltip.
* Customization: You can set preferences to alter app behaviour as you like.


[**Home Page**](http://www.aurvan.com/selecon/)


[**Download Latest Version**](http://www.aurvan.com/selecon/download-selecon-macosx-latest/)
